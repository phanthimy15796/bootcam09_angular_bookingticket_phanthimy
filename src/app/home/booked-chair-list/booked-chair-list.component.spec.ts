import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookedChairListComponent } from './booked-chair-list.component';

describe('BookedChairListComponent', () => {
  let component: BookedChairListComponent;
  let fixture: ComponentFixture<BookedChairListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookedChairListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookedChairListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
