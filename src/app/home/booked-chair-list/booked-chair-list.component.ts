import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-booked-chair-list',
  templateUrl: './booked-chair-list.component.html',
  styleUrls: ['./booked-chair-list.component.css']
})
export class BookedChairListComponent implements OnInit {
@Output() eventDeleteChair = new EventEmitter();
@Input() arrBookedChair:chair[] | undefined;
  countBooking:number = 0;
  total : number =0;
  constructor() { }

  ngOnInit(): void {
  }
  deleteChair(bookedChair:chair){
    this.eventDeleteChair.emit(bookedChair);
  }
  ngDoCheck(){
    if(typeof (this.arrBookedChair)!='undefined'){
      this.countBooking = this.arrBookedChair.reduce((total)=>{
          return total+=1;
      },0);

      this.total = this.arrBookedChair.reduce((total,item)=>{
        return total += item.gia;
      },0)
    }

  }

}
interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
}
