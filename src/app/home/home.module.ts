import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChairListComponent } from './chair-list/chair-list.component';
import { ChairItemComponent } from './chair-item/chair-item.component';
import { BookedChairListComponent } from './booked-chair-list/booked-chair-list.component';
import { HomeComponent } from './home.component';



@NgModule({
  declarations: [
    ChairListComponent,
    ChairItemComponent,
    BookedChairListComponent,
    HomeComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[HomeComponent],
})
export class HomeModule { }
