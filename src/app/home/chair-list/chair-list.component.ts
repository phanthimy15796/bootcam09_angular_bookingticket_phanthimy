import { Component, OnInit } from '@angular/core';

let arrChair:chair[] = [];
for (let index = 1; index <= 36; index++) {
  let trangThai = false;
  if(index === 10 || index === 31  || index === 36){
    trangThai = true;
  }
  arrChair.push({
    soGhe:index,
    tenGhe:`Ghế ${index}`,
    gia:100,
    trangThai,
  })
}
@Component({
  selector: 'app-chair-list',
  templateUrl: './chair-list.component.html',
  styleUrls: ['./chair-list.component.css']
})
export class ChairListComponent implements OnInit {
  arrBookedChair:chair[] = [];
  constructor() { }
  chairList = arrChair;
  ngOnInit(): void {
  }
  addChairParent(chairClick:chair){

    const index:number = this.arrBookedChair.findIndex((item:chair)=>{
      return item.soGhe === chairClick.soGhe;

    }
    )
    if(index === -1){

      this.arrBookedChair.push(chairClick);

    }else{
      this.arrBookedChair.splice(index,1);
    }

  }
}
interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
  isBooking?:boolean,
}
