import { Component, OnInit ,Input,EventEmitter,Output} from '@angular/core';

@Component({
  selector: 'app-chair-item',
  templateUrl: './chair-item.component.html',
  styleUrls: ['./chair-item.component.css']
})
export class ChairItemComponent implements OnInit {
  @Input() chair!:chair
  @Input() arrBookedChair!:chair[]
  @Output() eventAddChair = new EventEmitter()
  isbooking:boolean = false;
  constructor() { }
  checkIsbooking (){
      const index = this.arrBookedChair.findIndex((item)=>{
          return item.soGhe === this.chair.soGhe;
      })
      if(index !== -1){
          this.isbooking = true;
      }else{
        this.isbooking = false;
      }
  }
  ngOnInit(): void {
  }
  ngDoCheck(){
    this.checkIsbooking();
  }
  addChair(chair:chair){
    this.eventAddChair.emit(chair);
  }
}
interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
  isBooking?:boolean,
}
