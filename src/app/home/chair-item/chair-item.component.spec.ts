import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChairItemComponent } from './chair-item.component';

describe('ChairItemComponent', () => {
  let component: ChairItemComponent;
  let fixture: ComponentFixture<ChairItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChairItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChairItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
