import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { ChairListComponent } from './chair-list/chair-list.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('bookedChairList') bookedChairList!: ChairListComponent
  constructor() { }
  arrBookedChair:chair[] = [];
  ngOnInit(): void {
  }
  ngDoCheck(){

    if(this.bookedChairList){
        this.arrBookedChair = this.bookedChairList.arrBookedChair;
    }
  }
  deleteChairParent(deleteChairClick:chair){
    console.log(deleteChairClick)
    const index:number = this.arrBookedChair.findIndex((item:chair)=>{
      return item.soGhe === deleteChairClick.soGhe;
    })
    if(index !== -1){
      this.arrBookedChair.splice(index,1);
    }
  }
}
interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
}

